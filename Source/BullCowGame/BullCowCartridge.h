// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Console/Cartridge.h"
#include "BullCowCartridge.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class BULLCOWGAME_API UBullCowCartridge : public UCartridge
{
	GENERATED_BODY()

	public:
	virtual void BeginPlay() override;
	virtual void OnInput(const FString& Input) override;
	virtual void SetupGame();
	virtual void EndGame();
	virtual bool IsIsogram(const FString& word) const;
	virtual void PrintHiddenWordInfo() const;

	private:
	FString HiddenWord;
	int32 WordLength;
	int32 Lives;
	bool bGameOver;

	TArray<FString> HiddenWords = {
			TEXT("a"),
			TEXT("about"),
			TEXT("all"),
			TEXT("also"),
			TEXT("and"),
			TEXT("as"),
			TEXT("at"),
			TEXT("be"),
			TEXT("because"),
			TEXT("but"),
			TEXT("by"),
			TEXT("can"),
			TEXT("come"),
			TEXT("could"),
			TEXT("day"),
			TEXT("do"),
			TEXT("even"),
			TEXT("find"),
			TEXT("first"),
			TEXT("for"),
			TEXT("from"),
			TEXT("get"),
			TEXT("give"),
			TEXT("go"),
			TEXT("have"),
			TEXT("he"),
			TEXT("her"),
			TEXT("here"),
			TEXT("him"),
			TEXT("his"),
			TEXT("how"),
			TEXT("I"),
			TEXT("if"),
			TEXT("in"),
			TEXT("into"),
			TEXT("it"),
			TEXT("its"),
			TEXT("just"),
			TEXT("know"),
			TEXT("like"),
			TEXT("look"),
			TEXT("make"),
			TEXT("man"),
			TEXT("many"),
			TEXT("me"),
			TEXT("more"),
			TEXT("my"),
			TEXT("new"),
			TEXT("no"),
			TEXT("not"),
			TEXT("now"),
			TEXT("of"),
			TEXT("on"),
			TEXT("one"),
			TEXT("only"),
			TEXT("or"),
			TEXT("other"),
			TEXT("our"),
			TEXT("out"),
			TEXT("people"),
			TEXT("say"),
			TEXT("see"),
			TEXT("she"),
			TEXT("so"),
			TEXT("some"),
			TEXT("take"),
			TEXT("tell"),
			TEXT("than"),
			TEXT("that"),
			TEXT("the"),
			TEXT("their"),
			TEXT("them"),
			TEXT("then"),
			TEXT("there"),
			TEXT("these"),
			TEXT("they"),
			TEXT("thing"),
			TEXT("think"),
			TEXT("this"),
			TEXT("those"),
			TEXT("time"),
			TEXT("to"),
			TEXT("two"),
			TEXT("up"),
			TEXT("use"),
			TEXT("very"),
			TEXT("want"),
			TEXT("way"),
			TEXT("we"),
			TEXT("well"),
			TEXT("what"),
			TEXT("when"),
			TEXT("which"),
			TEXT("who"),
			TEXT("will"),
			TEXT("with"),
			TEXT("would"),
			TEXT("year"),
			TEXT("you"),
			TEXT("your")
	};

};