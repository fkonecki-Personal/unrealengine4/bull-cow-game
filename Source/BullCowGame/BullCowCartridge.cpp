// Fill out your copyright notice in the Description page of Project Settings.
#include <random>
#include <Math/UnrealMathUtility.h>
#include "BullCowCartridge.h"
#include <Misc/FileHelper.h>

void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();
    PrintLine(TEXT("*Press Tab to enter terminal\nHello player!Welcome to BullCow game.\n"));
     
    //const FString WordListhPath = FPaths::ProjectDir() / TEXT("Words.txt");
    //FFileHelper::LoadFileToStringArray(HiddenWords, *WordListhPath);
    SetupGame();
}

void UBullCowCartridge::OnInput(const FString& Input) // When the player hits enter
{
    ClearScreen();

    if (bGameOver ) {
        SetupGame();
    } 
    else if (Input.Len() != WordLength || !IsIsogram(Input)) 
        {
		    PrintLine(TEXT("Word must contain exactly %i letters and contain no repeating letters!\n"), WordLength);
            PrintHiddenWordInfo();
        }
        else
        {
            if (Input.Equals(HiddenWord)) {
                PrintLine(TEXT("You guessed correctly!\n"));
                SetupGame();
            } else {
                PrintLine(TEXT("You're guess was incorrect! %i lives left...\n"), --Lives);
                PrintHiddenWordInfo();
            }

            if (Lives == 0) {
                EndGame();
            }
        }
}

void UBullCowCartridge::SetupGame() 
{
    //int32 randIndex = FMath::RandRange(0, 99);
    HiddenWord = HiddenWords[0];

    /*
    while (!IsIsogram(HiddenWord)) 
    {
        HiddenWord = HiddenWords[(++randIndex) % 100];
    }
    */

    WordLength = HiddenWord.Len();
    Lives = 3;
    bGameOver = false;

    PrintHiddenWordInfo();
}

void UBullCowCartridge::EndGame() 
{
    ClearScreen();

    bGameOver = true;
    PrintLine(TEXT("Game over! Hidden word was: "));
    PrintLine(HiddenWord);
    PrintLine(TEXT("\nPress enter to try again.\n"));
}

bool UBullCowCartridge::IsIsogram(const FString& word) const
{
    for (int i = 0; i < word.Len() - 1; i++) 
    {
        for (int j = i + 1; j < word.Len(); j++) 
        {
            if (word[i] == word[j]) return false;
        }
    }
    return true;
}

void UBullCowCartridge::PrintHiddenWordInfo() const{
    PrintLine(TEXT("Guess the hidden word with %i letters.\n"), WordLength);
    PrintLine(TEXT("First letter of the hidden word is: %c\n"), HiddenWord[0]);
}
